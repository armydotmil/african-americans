$(document).ready(function() {
	addThis();
});

function addThis(){
    addthis.layers({
                'theme' : 'transparent',
                'share' : {
                  'position' : 'left',
                  'services' : 'facebook,twitter,google_plusone_share,pinterest',
                  'offset' : {'top' : '190px'}
                },
                'thankyou' : 'false',
            });
}